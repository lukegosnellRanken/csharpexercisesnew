﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InchesToCentimetersGUI
{
    public partial class FormInchesToCentimetersGUI : Form
    {
        public FormInchesToCentimetersGUI()
        {
            InitializeComponent();
        }

        //declare and initialize global program constants
        const double CENTIMETERSINANINCH = 2.54;

        private void textBoxInches_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            //declare and init local variables
            double inches = 0.0;
            double centimeters = 0.0;

            textBoxCentimeters.Text = "";

            //Check for numeric value in inches textbox
            if (isNumeric(textBoxInches.Text))
            {
                inches = Convert.ToDouble(textBoxInches.Text);
                centimeters = inches * CENTIMETERSINANINCH;
                textBoxCentimeters.Text = centimeters.ToString("f2");
            }
            else
            {
                MessageBox.Show("Blank or non-numeric input!",
                "Invalid input. Please Re-enter",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
                textBoxInches.Text = "";
                textBoxInches.Focus();
            }
        }

        private bool isNumeric(String input)
        {
            double test;
            return double.TryParse(input, out test);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxInches.Text = "";
            textBoxCentimeters.Text = "";
            textBoxInches.Focus();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Exit the program now!",
                "EXIT PROGRAM???",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Information) ==
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
