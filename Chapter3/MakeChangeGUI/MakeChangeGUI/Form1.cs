﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MakeChangeGUI
{
    public partial class MakeChangeGUI : Form
    {
        public MakeChangeGUI()
        {
            InitializeComponent();
        }

        private void textBoxTotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            int total = 0;
            int remainder = 0;
            int twenties = 0;
            int tens = 0;
            int fives = 0;
            int ones = 0;

            textBoxTwenties.Text = "";
            textBoxTens.Text = "";
            textBoxFives.Text = "";
            textBoxOnes.Text = "";

            if (isNumeric(textBoxTotal.Text))
            {
                total = Convert.ToInt32(textBoxTotal.Text);
                twenties = total / 20;
                remainder = total % 20;
                tens = remainder / 10;
                remainder = remainder % 10;
                fives = remainder / 5;
                remainder = remainder % 5;
                ones = remainder;

                textBoxTwenties.Text = twenties.ToString();
                textBoxTens.Text = tens.ToString();
                textBoxFives.Text = fives.ToString();
                textBoxOnes.Text = ones.ToString();
            }
            else
            {
                MessageBox.Show("Blank or non-numeric input!",
                "Invalid input. Please Re-enter",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
                textBoxTotal.Text = "";
                textBoxTotal.Focus();
            }
        }


        private bool isNumeric(String input)
        {
            double test;
            return double.TryParse(input, out test);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxTotal.Text = "";
            textBoxTwenties.Text = "";
            textBoxTens.Text = "";
            textBoxFives.Text = "";
            textBoxOnes.Text = "";
            textBoxTotal.Focus();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Exit the program now!",
                "EXIT PROGRAM???",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Information) ==
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
