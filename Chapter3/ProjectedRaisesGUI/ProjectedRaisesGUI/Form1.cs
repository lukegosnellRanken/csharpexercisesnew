﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectedRaisesGUI
{
    public partial class ProjectedRaisesGUI : Form
    {
        public ProjectedRaisesGUI()
        {
            InitializeComponent();
        }

        const double RAISE = .04;
        

        private void textBoxSalary_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            double salary = 0.0;
            double salaryNew = 0.0;

            textBoxSalaryNew.Text = "";

            if (isNumeric(textBoxSalary.Text))
            {
                salary = Convert.ToDouble(textBoxSalary.Text);
                salaryNew = (salary + (salary * RAISE));
                textBoxSalaryNew.Text = salaryNew.ToString("C");
            }
            else
            {
                MessageBox.Show("Blank or non-numeric input!",
                "Invalid input. Please Re-enter",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error);
                textBoxSalary.Text = "";
                textBoxSalary.Focus();
            }
        }

        private bool isNumeric(String input)
        {
            double test;
            return double.TryParse(input, out test);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxSalary.Text = "";
            textBoxSalaryNew.Text = "";
            textBoxSalary.Focus();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Exit the program now!",
                "EXIT PROGRAM???",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Information) ==
                DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
