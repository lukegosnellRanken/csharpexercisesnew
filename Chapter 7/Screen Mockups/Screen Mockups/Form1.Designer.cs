﻿namespace Screen_Mockups
{
    partial class FormScreenMokups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelAccountNumber = new System.Windows.Forms.Label();
            this.labelPinNumber = new System.Windows.Forms.Label();
            this.textBoxAccountNumber = new System.Windows.Forms.TextBox();
            this.textBoxPinNumber = new System.Windows.Forms.TextBox();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.labelAccountBalance = new System.Windows.Forms.Label();
            this.textBoxDeposit = new System.Windows.Forms.TextBox();
            this.textBoxWithdrawal = new System.Windows.Forms.TextBox();
            this.labelMakeADeposit = new System.Windows.Forms.Label();
            this.labelMakeAWithdrawal = new System.Windows.Forms.Label();
            this.labelOnlineBank = new System.Windows.Forms.Label();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonDeposit = new System.Windows.Forms.Button();
            this.buttonWithdrawal = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelAccountNumber
            // 
            this.labelAccountNumber.AutoSize = true;
            this.labelAccountNumber.Location = new System.Drawing.Point(44, 85);
            this.labelAccountNumber.Name = "labelAccountNumber";
            this.labelAccountNumber.Size = new System.Drawing.Size(90, 13);
            this.labelAccountNumber.TabIndex = 0;
            this.labelAccountNumber.Text = "Account Number:";
            // 
            // labelPinNumber
            // 
            this.labelPinNumber.AutoSize = true;
            this.labelPinNumber.Location = new System.Drawing.Point(106, 133);
            this.labelPinNumber.Name = "labelPinNumber";
            this.labelPinNumber.Size = new System.Drawing.Size(28, 13);
            this.labelPinNumber.TabIndex = 1;
            this.labelPinNumber.Text = "PIN:";
            // 
            // textBoxAccountNumber
            // 
            this.textBoxAccountNumber.Location = new System.Drawing.Point(153, 78);
            this.textBoxAccountNumber.Name = "textBoxAccountNumber";
            this.textBoxAccountNumber.Size = new System.Drawing.Size(195, 20);
            this.textBoxAccountNumber.TabIndex = 2;
            // 
            // textBoxPinNumber
            // 
            this.textBoxPinNumber.Location = new System.Drawing.Point(153, 126);
            this.textBoxPinNumber.Name = "textBoxPinNumber";
            this.textBoxPinNumber.Size = new System.Drawing.Size(145, 20);
            this.textBoxPinNumber.TabIndex = 3;
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.Location = new System.Drawing.Point(106, 175);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(52, 13);
            this.labelWelcome.TabIndex = 4;
            this.labelWelcome.Text = "Welcome";
            // 
            // labelAccountBalance
            // 
            this.labelAccountBalance.AutoSize = true;
            this.labelAccountBalance.Location = new System.Drawing.Point(106, 216);
            this.labelAccountBalance.Name = "labelAccountBalance";
            this.labelAccountBalance.Size = new System.Drawing.Size(122, 13);
            this.labelAccountBalance.TabIndex = 5;
            this.labelAccountBalance.Text = "Your account balance is";
            // 
            // textBoxDeposit
            // 
            this.textBoxDeposit.Location = new System.Drawing.Point(105, 289);
            this.textBoxDeposit.Name = "textBoxDeposit";
            this.textBoxDeposit.Size = new System.Drawing.Size(139, 20);
            this.textBoxDeposit.TabIndex = 6;
            // 
            // textBoxWithdrawal
            // 
            this.textBoxWithdrawal.Location = new System.Drawing.Point(105, 349);
            this.textBoxWithdrawal.Name = "textBoxWithdrawal";
            this.textBoxWithdrawal.Size = new System.Drawing.Size(139, 20);
            this.textBoxWithdrawal.TabIndex = 7;
            // 
            // labelMakeADeposit
            // 
            this.labelMakeADeposit.AutoSize = true;
            this.labelMakeADeposit.Location = new System.Drawing.Point(102, 257);
            this.labelMakeADeposit.Name = "labelMakeADeposit";
            this.labelMakeADeposit.Size = new System.Drawing.Size(170, 13);
            this.labelMakeADeposit.TabIndex = 8;
            this.labelMakeADeposit.Text = "Would you like to make a deposit?";
            // 
            // labelMakeAWithdrawal
            // 
            this.labelMakeAWithdrawal.AutoSize = true;
            this.labelMakeAWithdrawal.Location = new System.Drawing.Point(102, 321);
            this.labelMakeAWithdrawal.Name = "labelMakeAWithdrawal";
            this.labelMakeAWithdrawal.Size = new System.Drawing.Size(186, 13);
            this.labelMakeAWithdrawal.TabIndex = 9;
            this.labelMakeAWithdrawal.Text = "Would you like to make a withdrawal?";
            // 
            // labelOnlineBank
            // 
            this.labelOnlineBank.AutoSize = true;
            this.labelOnlineBank.Font = new System.Drawing.Font("High Tower Text", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOnlineBank.Location = new System.Drawing.Point(30, 20);
            this.labelOnlineBank.Name = "labelOnlineBank";
            this.labelOnlineBank.Size = new System.Drawing.Size(374, 32);
            this.labelOnlineBank.TabIndex = 10;
            this.labelOnlineBank.Text = "Welcome to the Online Bank!";
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(329, 123);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(75, 23);
            this.buttonLogin.TabIndex = 11;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonDeposit
            // 
            this.buttonDeposit.Location = new System.Drawing.Point(289, 286);
            this.buttonDeposit.Name = "buttonDeposit";
            this.buttonDeposit.Size = new System.Drawing.Size(75, 23);
            this.buttonDeposit.TabIndex = 12;
            this.buttonDeposit.Text = "Deposit";
            this.buttonDeposit.UseVisualStyleBackColor = true;
            // 
            // buttonWithdrawal
            // 
            this.buttonWithdrawal.Location = new System.Drawing.Point(289, 346);
            this.buttonWithdrawal.Name = "buttonWithdrawal";
            this.buttonWithdrawal.Size = new System.Drawing.Size(75, 23);
            this.buttonWithdrawal.TabIndex = 13;
            this.buttonWithdrawal.Text = "Withdraw";
            this.buttonWithdrawal.UseVisualStyleBackColor = true;
            // 
            // buttonLogout
            // 
            this.buttonLogout.Location = new System.Drawing.Point(142, 416);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(156, 23);
            this.buttonLogout.TabIndex = 14;
            this.buttonLogout.Text = "Logout";
            this.buttonLogout.UseVisualStyleBackColor = true;
            // 
            // FormScreenMokups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(435, 472);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.buttonWithdrawal);
            this.Controls.Add(this.buttonDeposit);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.labelOnlineBank);
            this.Controls.Add(this.labelMakeAWithdrawal);
            this.Controls.Add(this.labelMakeADeposit);
            this.Controls.Add(this.textBoxWithdrawal);
            this.Controls.Add(this.textBoxDeposit);
            this.Controls.Add(this.labelAccountBalance);
            this.Controls.Add(this.labelWelcome);
            this.Controls.Add(this.textBoxPinNumber);
            this.Controls.Add(this.textBoxAccountNumber);
            this.Controls.Add(this.labelPinNumber);
            this.Controls.Add(this.labelAccountNumber);
            this.Name = "FormScreenMokups";
            this.Text = "Online Bank";
            this.Load += new System.EventHandler(this.FormScreenMokups_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAccountNumber;
        private System.Windows.Forms.Label labelPinNumber;
        private System.Windows.Forms.TextBox textBoxAccountNumber;
        private System.Windows.Forms.TextBox textBoxPinNumber;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.Label labelAccountBalance;
        private System.Windows.Forms.TextBox textBoxDeposit;
        private System.Windows.Forms.TextBox textBoxWithdrawal;
        private System.Windows.Forms.Label labelMakeADeposit;
        private System.Windows.Forms.Label labelMakeAWithdrawal;
        private System.Windows.Forms.Label labelOnlineBank;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonDeposit;
        private System.Windows.Forms.Button buttonWithdrawal;
        private System.Windows.Forms.Button buttonLogout;
    }
}

