﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Screen_Mockups
{
    public partial class FormScreenMokups : Form
    {

        const int ARRAYSIZE = 5;

        static string[] accountNumbers = { "1153411", "123423", "343464", "928502", "175037" };
        static string[] pinNumbers = { "4932", "9204", "9253", "9195", "1574" };
        static string[] accountNames = { "Smith", "Joel", "Young", "Robinson", "Puff" };
        static string[] accountBalances = { "10000", "1200", "2000", "1000", "20000" };

        public FormScreenMokups()
        {
            InitializeComponent();
        }

        private void FormScreenMokups_Load(object sender, EventArgs e)
        {
            textBoxAccountNumber.Text = "";
            textBoxPinNumber.Text = "";
            textBoxDeposit.Text = "";
            textBoxWithdrawal.Text = "";
            textBoxAccountNumber.Focus();
            labelWelcome.Text = "";
            labelAccountBalance.Text = "";
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            validateAccount();
        }

        public void validateAccount()
        {
            bool isValid = false;

            for (int i = 0; i < accountNumbers.Length; i++)
            {
                if (accountNumbers.Contains(textBoxAccountNumber.Text))
                {
                    if (pinNumbers[i].Contains(textBoxPinNumber.Text))
                    {
                        isValid = true;
                        labelWelcome.Text = ("Welcome Mr. " + accountNames[i] + "!");
                        labelAccountBalance.Text = ("Your account balance is " + accountBalances[i].ToString("C"));
                    }
                }
            }
            
            if (isValid == false)
            {
                displayError();
            }
        }

        public void displayError()
        {
            textBoxAccountNumber.Text = "";
            textBoxPinNumber.Text = "";
            labelWelcome.Text = ("Invalid account or pin number.");
            labelAccountBalance.Text = ("");
        }
    }
}
