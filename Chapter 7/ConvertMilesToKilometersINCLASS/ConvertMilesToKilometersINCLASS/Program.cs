﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ConvertMilesToKilometers
{
    class Program
    {
        const double KILOSINAMILE = 1.60934;


        static void Main(string[] args)
        {
            double miles = 0.0;
            double kilos = 0.0;
            string mstr = "";

            Write("Please enter miles: ");
            mstr = ReadLine();

            if (isNumeric(mstr))
            {
                miles = Convert.ToDouble(mstr);
                kilos = convertMilesToKilometers(miles);
            }

            WriteLine("There are " + kilos.ToString("f2") + " kilometers in " + miles.ToString("f2") + " miles.");
            ReadLine();
        }

        static double convertMilesToKilometers(double miles)
        {
            return miles * KILOSINAMILE;
        }

        static bool isNumeric(String input)
        {
            double test;
            return double.TryParse(input, out test);
        }
    }
}