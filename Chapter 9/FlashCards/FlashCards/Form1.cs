﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlashCards
{
    public partial class FormFlashCards : Form
    {
        public class FlashCard
        {
            //Add instance variables
            private string _term;
            private string _definition;

            //Constructor
            public FlashCard(String term, String definition)
            {
                _term = term;
                _definition = definition;
            }

            //methods to get the data (instance fields)
            public string GetTerm()
            {
                return _term;
            }

            public string GetDefinition()
            {
                return _definition;
            }
        }

        FlashCard[] myCards = new FlashCard[20];

        int numCards = 5;

        public FormFlashCards()
        {
            InitializeComponent();
            myCards[0] = new FlashCard("HTML", "Hypertext Markup Language");
            myCards[1] = new FlashCard("CSS", "Cascading Style Sheets");
            myCards[2] = new FlashCard("JS", "JavaScript");
            myCards[3] = new FlashCard("C#", "C Sharp");
            myCards[4] = new FlashCard("SQL", "Structured Query Language");
        }

        private void buttonDefine_Click(object sender, EventArgs e)
        {
            string userInput = textBoxDefine.Text;
            bool found = false;

            for (int i = 0; i < numCards; i++)
            {
                if (myCards[i].GetTerm().Equals(userInput))
                {
                    labelTermDefinition.Text = myCards[i].GetDefinition();
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                labelTermDefinition.Text = "I don't know that term yet. What does it mean?";
                buttonAdd.Visible = true;
                textBoxAdd.Visible = true;
                textBoxDefine.Visible = true;
            }

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            myCards[numCards] = new FlashCard(textBoxAdd.Text, textBoxDefine.Text);
            numCards++;
            labelDefineTerm.Text = "Term Added";
            buttonAdd.Visible = false;
            textBoxDefine.Visible = false;
        }
    }
}
