﻿namespace FlashCards
{
    partial class FormFlashCards
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDefine = new System.Windows.Forms.TextBox();
            this.textBoxAdd = new System.Windows.Forms.TextBox();
            this.buttonDefine = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.labelDefineTerm = new System.Windows.Forms.Label();
            this.labelTermDefinition = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxDefine
            // 
            this.textBoxDefine.Location = new System.Drawing.Point(101, 172);
            this.textBoxDefine.Name = "textBoxDefine";
            this.textBoxDefine.Size = new System.Drawing.Size(508, 20);
            this.textBoxDefine.TabIndex = 0;
            // 
            // textBoxAdd
            // 
            this.textBoxAdd.Location = new System.Drawing.Point(101, 363);
            this.textBoxAdd.Name = "textBoxAdd";
            this.textBoxAdd.Size = new System.Drawing.Size(508, 20);
            this.textBoxAdd.TabIndex = 1;
            this.textBoxAdd.Visible = false;
            // 
            // buttonDefine
            // 
            this.buttonDefine.Location = new System.Drawing.Point(641, 169);
            this.buttonDefine.Name = "buttonDefine";
            this.buttonDefine.Size = new System.Drawing.Size(75, 23);
            this.buttonDefine.TabIndex = 2;
            this.buttonDefine.Text = "Define";
            this.buttonDefine.UseVisualStyleBackColor = true;
            this.buttonDefine.Click += new System.EventHandler(this.buttonDefine_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(641, 363);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 3;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Visible = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // labelDefineTerm
            // 
            this.labelDefineTerm.AutoSize = true;
            this.labelDefineTerm.Location = new System.Drawing.Point(98, 125);
            this.labelDefineTerm.Name = "labelDefineTerm";
            this.labelDefineTerm.Size = new System.Drawing.Size(184, 13);
            this.labelDefineTerm.TabIndex = 4;
            this.labelDefineTerm.Text = "What term do you want me to define?";
            // 
            // labelTermDefinition
            // 
            this.labelTermDefinition.AutoSize = true;
            this.labelTermDefinition.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTermDefinition.Location = new System.Drawing.Point(96, 269);
            this.labelTermDefinition.Name = "labelTermDefinition";
            this.labelTermDefinition.Size = new System.Drawing.Size(0, 25);
            this.labelTermDefinition.TabIndex = 6;
            // 
            // FormFlashCards
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelTermDefinition);
            this.Controls.Add(this.labelDefineTerm);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonDefine);
            this.Controls.Add(this.textBoxAdd);
            this.Controls.Add(this.textBoxDefine);
            this.Name = "FormFlashCards";
            this.Text = "Flash Cards";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDefine;
        private System.Windows.Forms.TextBox textBoxAdd;
        private System.Windows.Forms.Button buttonDefine;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Label labelDefineTerm;
        private System.Windows.Forms.Label labelTermDefinition;
    }
}

