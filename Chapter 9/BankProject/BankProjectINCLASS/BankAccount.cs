﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankProjectINCLASS
{
    class BankAccount : IComparable
    {

        const string DEFFN = "UFirst";
        const string DEFLN = "ULast";
        const double DEFINITB = 25;
        const int DEFLACCTN = 1000;
        const int DEFUACCTNUM = 10000;

        string firstName;
        string lastName;
        int acctNumber;
        double initialBal;

        double currentBal = 0;

        Random rand = new Random();

        public BankAccount()
        {
            FillWithDefaults();
        }

        public BankAccount(string fn, string ln, int an, double ib)
        {
            validateFirstName(fn);
            validateLastName(ln);
            validateAcctNumber(an);
            validateInitialBalance(ib);
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                validateFirstName(value);
            }
        }


        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                validateLastName(value);
            }
        }

        public int AcctNumber
        {
            get
            {
                return acctNumber;
            }
            set
            {
                validateAcctNumber(value);
            }
        }

        public double InitBalance
        {
            get
            {
                return initialBal;
            }
            set
            {
                validateInitialBalance(value);
            }
        }



        public void FillWithDefaults()
        {
            firstName = DEFFN;
            lastName = DEFLN;
            acctNumber = rand.Next(DEFLACCTN, DEFUACCTNUM);
            initialBal = DEFINITB;
        }

        // ? = accept --- : = otherwise, do this

        public void validateFirstName(string fn)
        {
            firstName = (fn != "") ? fn : DEFFN;
        }

        public void validateLastName(string ln)
        {
            lastName = (ln != "") ? ln : DEFLN;
        }

        public void validateAcctNumber(int an)
        {
            acctNumber = ((an >= DEFLACCTN) && (an <= DEFUACCTNUM)) ? an : (rand.Next(DEFLACCTN, DEFUACCTNUM));
        }

        public void validateInitialBalance(double ib)
        {
            initialBal = (ib >= DEFINITB) ? ib : DEFINITB;
        }


        //Overloaded ToString() method
        public override string ToString()
        {
            string  str = "\nFirst Name:\t\t" + firstName;
                    str += "\nLast Name:\t\t" + lastName;
                    str += "\nAccount Number:\t\t" + acctNumber;
                    str += "\nInitial Balance:\t\t" + initialBal;
            return str;

        }

        public void deposit(double d)
        {
            currentBal = (d > 0) ? currentBal += d : currentBal;
        }

        public void withdrawal(double w)
        {
            double temp = currentBal - w;

            currentBal = (temp >= DEFINITB) ? temp : currentBal;
        }

        int IComparable.CompareTo(object o)
        {
            int retVal;
            BankAccount temp = (BankAccount)o;
            retVal = this.LastName.CompareTo(temp.LastName);
            return retVal;
        }
    }
}
