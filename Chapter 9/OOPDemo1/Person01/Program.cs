﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Person01
{
    class Program
    {
        static void Main(string[] args)
        {
            Person jeff = new Person();

            Person sandy = new Person("Sandy", "Scott");

            Person01 taylor = new Person01("Taylor", "Scott", "111-22-3333", 66, 0, "Brown", "Blonde", 'F');

            Person01 kenzie = new Person("Kenzie", "Scott", "222-22-3333", 97, 100, "Brown", 'F');

            Person01 chloe = new Person01("Chloe", "Scott", "333-44-5555", 69, 125, "Blue", "Blond", 'F');

            Console.Clear();
            ReadLine();
            WriteLine(chloe);
            ReadLine();
        }
    }

    class Person
    {
        const string DEFFN = "UnknownFN";
        const string DEFLN = "UnknownLN";
        const string DEFSSN = "xxx-xx-xxxx";
        const int MINW = 1;
        const int MAXW = 777;
        const int DEFW = 175;
        const int MINH = 12;
        const int MAXH = 96;
        const int DEFH = 72;
        const string DEFEC = "Blue";
        const string DEFHC = "Brown";
        const char DEFG = 'F';

        string fname;
        string lname;
        string ssn;
        int height;
        int weight;
        string eyeColor;
        string hairColor;
        char gender;

        public static int personCount = 0;

        public Person()
        {
            ++personCount;
            fillNamesWithDefaultValues();
            fillPersonWithDefaultValues();
        }

        public Person(string fn, string ln)
        {
            ++personCount;
            fname = fn;
            lname = ln;
            fillPersonWithDefaultValues();
        }

        public Person(string fn, string ln, string soc, int h, int w, string ec, string hc, char g)
        {
            ++personCount;
            fname = fn;
            lname = ln;
            ssn = soc;
            validateHeight;
            validateWeight(w);
            eyecolor = ec;
            hairColor = hc;
            gender = g;
        }

        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Ssn { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public string EyeColor { get; set; }
        public string HairColor { get; set; }
        public char Gender { get; set; }

        public void fillNamesWithDefaultValues()
        {
            fname = DEFFN;
            lname = DEFLN;
            fillPersonWithDefaultValues();
        }

        public void fillPersonWithDefaultValues()
        {
            ssn = DEFSSN;
            height = DEFH;
            weight = DEFW;
            eyeColor = DEFEC;
            hairColor = DEFHC;
            gender = DEFG;
        }

        public void validateHeight(int h)
        {
            height = ((h >= MINH) &&
                     (h <= MAXH)) ? h
                                  : DEFH;
        }

        public void validateWeight(int w)
        {
            weight = ((h >= MINW) &&
                     (h <= MAXW)) ? w
                                  : DEFW;
        }

        public override string ToString()
        {
            string str = "\nFirst Name: " + fname;
            str += "\nLast Name: " + lname;
            str += "\nSoc Sec Num: " + ssn;
            str += "\nHeight: " + height;
            str += "\nWidth: " + weight;
            str += "\nEye Color: " + eyeColor;
            str += "\nHair Color: " + hairColor;
            str += "\nGender: " + gender;

            return str;
        }
    }
}
