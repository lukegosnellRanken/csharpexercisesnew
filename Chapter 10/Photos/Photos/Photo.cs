﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photos
{
    public class Photo
    {
        //instance fields
        protected float _width;
        protected float _height;

        public Photo(float width, float height)
        {
            _width = width;
            _height = height;
        }

        //properties
        public float Width
        {
            get { return _width;  }
            set { _width = value; }
        }

        public float Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public virtual float Price
        {
            get
            {
                if(_width == 8 && _height == 10)
                {
                    return 3.99F;
                }
                else if(_width == 10 && _height == 12)
                {
                    return 5.99F;
                }
                else
                {
                    return 9.99F;
                }
            }
        }

        public override string ToString()
        {
            return String.Format("{0}x{1} Photo", _width, _height);
        }
    }
}
