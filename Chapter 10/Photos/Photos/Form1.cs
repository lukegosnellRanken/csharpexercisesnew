﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Photos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            if (radioButtonUnframed.Checked)
            {
                Photo myPhoto = new Photo(Convert.ToSingle(textBoxWidth.Text), Convert.ToSingle(textBoxHeight.Text));
                labelInfo.Text = myPhoto.ToString();
                labelCost.Text = String.Format("Cost: {0}", myPhoto.Price.ToString("C"));
            }
            else if (radioButtonMatted.Checked)
            {
                Color theChosenColor;
                
                if (radioButtonBlack.Checked)
                {
                    theChosenColor = Color.BLACK;
                }
                else if (radioButtonRed.Checked)
                {
                    theChosenColor = Color.RED;
                }
                else if (radioButtonGreen.Checked)
                {
                    theChosenColor = Color.GREEN;
                }
                else if (radioButtonBlue.Checked)
                {
                    theChosenColor = Color.BLUE;
                }
                else
                {
                    theChosenColor = Color.WHITE;
                }

                MattedPhoto myMattedPhoto = new MattedPhoto(Convert.ToSingle(textBoxWidth.Text), Convert.ToSingle(textBoxHeight.Text), theChosenColor);
                labelInfo.Text = myMattedPhoto.ToString();
                labelCost.Text = String.Format("Cost: {0}", myMattedPhoto.Price.ToString("C"));
            }
            else if (radioButtonFramed.Checked)
            {
                Material theChosenMaterial;
                Style theChosenStyle;

                if (radioButtonPine.Checked)
                {
                    theChosenMaterial = Material.PINE;
                }
                else if (radioButtonOak.Checked)
                {
                    theChosenMaterial = Material.OAK;
                }
                else if (radioButtonSteel.Checked)
                {
                    theChosenMaterial = Material.STEEL;
                }
                else if (radioButtonSilver.Checked)
                {
                    theChosenMaterial = Material.SILVER;
                }
                else
                {
                    theChosenMaterial = Material.GOLD;
                }

                if (radioButtonSimple.Checked)
                {
                    theChosenStyle = Style.SIMPLE;
                }
                else if (radioButtonModern.Checked)
                {
                    theChosenStyle = Style.MODERN;
                }
                else if (radioButtonAntique.Checked)
                {
                    theChosenStyle = Style.ANTIQUE;
                }
                else if (radioButtonVintage.Checked)
                {
                    theChosenStyle = Style.VINTAGE;
                }
                else
                {
                    theChosenStyle = Style.ECLECTIC;
                }

                FramedPhoto myFramedPhoto = new FramedPhoto(Convert.ToSingle(textBoxWidth.Text), Convert.ToSingle(textBoxHeight.Text), theChosenMaterial, theChosenStyle);
                labelInfo.Text = myFramedPhoto.ToString();
                labelCost.Text = String.Format("Cost: {0}", myFramedPhoto.Price.ToString("C"));

            }

        }

        private void radioButtonMatted_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxColor.Enabled = true;
            groupBoxStyle.Enabled = false;
            groupBoxMaterial.Enabled = false;
        }

        private void radioButtonUnFramed_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxColor.Enabled = false;
            groupBoxStyle.Enabled = false;
            groupBoxMaterial.Enabled = false;
        }

        private void radioButtonFramed_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxColor.Enabled = false;
            groupBoxStyle.Enabled = true;
            groupBoxMaterial.Enabled = true;
        }
    }
}
