﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ArrayDemo
{
    class Program
    {
        const int ARRAYSIZE = 10;
        const int MINVAL = 1;
        const int MAXVAL = 100;

        static int[] numArray = new int[ARRAYSIZE];
        static int[] origArray = new int[ARRAYSIZE];
        static Random random = new Random();

        static void Main(string[] args)
        {
            fillArray();

            for (; ; )
            {
                presentMenu();
            }
        }

        static void fillArray()
        {
            for (int lcv = 0; lcv < numArray.Length; ++lcv)
            {
                origArray[lcv] = generateRandomNumber();
                numArray[lcv] = origArray[lcv];
            }
        }

        static int generateRandomNumber()
        {
            return random.Next(1, 101);
        }

        static void presentMenu()
        {

            int menuChoice = 0;

            WriteLine("\nEnter a 1 to view array in ascending order");
            WriteLine("\nEnter a 2 to view array in descending order");
            WriteLine("\nEnter a 3 to view contents of a specific array element");
            WriteLine("\nEnter a 4 to quit");
            Write("Enter a 1, 2, 3, or 4");
            menuChoice = Convert.ToInt16(ReadLine());

            while ((menuChoice != 1) && (menuChoice != 2) && (menuChoice != 3) && (menuChoice != 4))
            {
                Console.Clear();
                presentMenu();
            }

            switch (menuChoice)
            {
                case 1:
                    showArrayAsc();
                    break;

                case 2:
                    showArrayDesc();
                    break;

                case 3:
                    showArrayElement();
                    break;

                case 4:
                    Environment.Exit(0);
                    break;

                default:
                    break;
            }
        }

        static void showArrayAsc()
        {
            Array.Sort(numArray);
            WriteLine("\nHere is the array in ascending order");

            for (int lcv = 0; lcv< numArray.Length; ++ lcv)
            {
                Write(numArray[lcv] + "\t");
            }

            WriteLine("");

        }

        static void showArrayDesc()
        {
            Array.Sort(numArray);
            Array.Reverse(numArray);
            WriteLine("\nHere is the array in descending order");

            for (int lcv = 0; lcv < numArray.Length; ++lcv)
            {
                Write(numArray[lcv] + "\t");
            }

            WriteLine("");

        }

        static void showArrayElement()
        {
            int elNumber = 0;

            WriteLine("\nUsing the array that is in its original order");

            WriteLine("\nEnter an arry element number " + "between " + MINVAL + " and 10" + "\nto view the contents of that array element");
            elNumber = Convert.ToInt32(ReadLine());

            while ((elNumber < MINVAL) || (elNumber > 10))
            {
                WriteLine("\nEnter an arry element number " + "between " + MINVAL + " and 10" + "\nto view the contents of that array element");
                elNumber = Convert.ToInt32(ReadLine());
            }

            WriteLine("\nThe content of numberArray element " + (elNumber - 1) + " are " + numArray[elNumber - 1]);
        }
    }
}
