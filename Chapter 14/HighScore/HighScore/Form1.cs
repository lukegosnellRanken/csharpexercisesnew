﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace HighScore
{
    public partial class highScore : Form
    {
        const int MINNUMBER = 1;
        const int MAXNUMBER = 3;
        const int MAXTRIES = 10;
        const string FILENAME = @"C:\Users\lukeg\Desktop\AWD 1100\csharpexercisesnew\Chapter 14\HighScore\HighScore\HighScore.txt";

        int[] randomChoices = new int[10];
        static int count = 0;
        static int numCorrect = 0;
        int previousHigh = 0;
        char userChoice = ' ';
        char computerChoice;
        static FileStream inputFile = new FileStream(FILENAME, FileMode.Open, FileAccess.Read);

        StreamReader reader = new StreamReader(inputFile);
        static FileStream outputFile;
        StreamWriter writer = null;
        string recordIn;

        public highScore()
        {
            InitializeComponent();
        }

        private void highScore_Load(object sender, EventArgs e)
        {
            Random rand = new Random();
            for (int lcv = 0; lcv < MAXTRIES; ++lcv)
            {
                randomChoices[lcv] = rand.Next(MINNUMBER, MAXNUMBER);
                labelHighScore.Text += " " + randomChoices[lcv];
            }

            recordIn = reader.ReadLine();
            previousHigh = Convert.ToInt32(recordIn);
            labelHighScore.Text += " " + previousHigh.ToString();
            reader.Close();
            inputFile.Close();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            determineUserChoice();

            determineComputerChoice();

            if (userChoice == computerChoice)
            {
                ++numCorrect;
            }

            labelUserChoice.Text += numCorrect.ToString() + "Correct Out Of " + count.ToString() + " Turns,";

            if (count == MAXTRIES)
            {
                disableTheGame();
                writeOutFinalStuff();
            }
        }

        private void determineUserChoice()
        {
            if (radioButtonA.Checked)
            {
                userChoice = 'A';
                radioButtonA.Checked = false;
            }
            else if (radioButtonB.Checked) {
                userChoice = 'B';
                radioButtonB.Checked = false;
            }
            else if (radioButtonC.Checked)
            {
                userChoice = 'C';
                radioButtonC.Checked = false;
            }
        }

        private void determineComputerChoice()
        {
            if (randomChoices[count] == 0)
            {
                computerChoice = 'A';
            }
            if (randomChoices[count] == 1)
            {
                computerChoice = 'B';
            }
            if (randomChoices[count] == 2)
            {
                computerChoice = 'C';
            }
        }

        private void disableTheGame()
        {
            radioButtonA.Enabled = false;
            radioButtonB.Enabled = false;
            radioButtonC.Enabled = false;
            buttonSubmit.Enabled = false;
        }

        private void writeOutFinalStuff()
        {
            outputFile = new FileStream(FILENAME, FileMode.Open, FileAccess.Write);

            writer = new StreamWriter(outputFile);

            if (numCorrect > previousHigh)
            {
                writer.WriteLine("" + numCorrect.ToString());
            }
            else
            {
                writer.WriteLine("" + previousHigh.ToString());
            }

            writer.Close();
            outputFile.Close();
        }
    }
}
