﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace CheckMonth
{
    class Program
    {
        static void Main(string[] args)
        {
            const int MAXMONTH = 12;
            const int MINMONTH = 1;
            const string INPUTOOR = "Out of range input.";

            int userMonth = 0;
            string userMonthTest = "";
            bool keepGoing = true;

            do
            {
                Write("\nEnter your birth month (1-12): ");
                userMonthTest = ReadLine();

                if (isNumeric(userMonthTest))
                {
                    userMonth = Convert.ToInt32(userMonthTest);

                    if (userMonth > MAXMONTH || userMonth < MINMONTH)
                    {
                        WriteLine("\n" + INPUTOOR);
                        
                    }

                    else
                    {
                        keepGoing = false;
                    }
                }



                else
                {
                    Write("\nNon-numeric input.\n");
                }

            } while (keepGoing == true);

            Write("\n" + userMonth.ToString() + " is a valid month.");
            ReadLine();
        }

        static private bool isNumeric(String input)
        {
            int test;
            return int.TryParse(input, out test);
        }
    }
}
