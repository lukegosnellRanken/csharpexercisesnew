﻿using System;
using static System.Console;

namespace ProjectedRaisesInteractive
{
    class Program
    {
        static void Main(string[] args)
        {
            const double RAISE = .04;
            double salary1 = 0.0;
            double salary2 = 0.0;
            double salary3 = 0.0;
            double salary1New = 0.0;
            double salary2New = 0.0;
            double salary3New = 0.0;

            Write("Enter first salary: ");
            salary1 = Convert.ToDouble(ReadLine());
            salary1New = salary1 + (salary1 * RAISE);

            Write("Enter second salary: ");
            salary2 = Convert.ToDouble(ReadLine());
            salary2New = salary2 + (salary2 * RAISE);

            Write("Enter third salary: ");
            salary3 = Convert.ToDouble(ReadLine());
            salary3New = salary3 + (salary3 * RAISE);

            WriteLine("\n\n\n\t\t Salary 1 Old: " + salary1.ToString("C"));
            WriteLine("\n\t\t Salary 2 Old: " + salary2.ToString("C"));
            WriteLine("\n\t\t Salary 3 Old: " + salary3.ToString("C"));

            WriteLine("\n\n\n\t\t Salary 1 New: " + salary1New.ToString("C"));
            WriteLine("\n\t\t Salary 2 New: " + salary2New.ToString("C"));
            WriteLine("\n\t\t Salary 3 New: " + salary3New.ToString("C"));

            ReadLine();
        }
    }
}
