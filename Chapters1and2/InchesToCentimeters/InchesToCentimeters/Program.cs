﻿using System;
using static System.Console;

namespace InchesToCentimeters
{
    class Program
    {
        static void Main(string[] args)
        {
            const double CENTIMETERSINANINCH = 2.54;
            double inches = 3;
            double centimeters = inches * CENTIMETERSINANINCH;

            WriteLine("\n\n\n\t\t" + inches.ToString() + " inches is " + centimeters.ToString() + " centimeters");

            ReadLine();
        }
    }
}
