﻿using System;
using static System.Console;

namespace MakeChange
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0;
            int remainder = 0;
            int twenties = 0;
            int tens = 0;
            int fives = 0;
            int ones = 0;

            Write("\n\n\n\t\tPlease enter dollar amount (no change): ");
            total = Convert.ToInt32(ReadLine());

            twenties = total / 20;
            remainder = total % 20;
            tens = remainder / 10;
            remainder = remainder % 10;
            fives = remainder / 5;
            remainder = remainder % 5;
            ones = remainder;


            WriteLine("\n\t\t " + total.ToString("C") + " is: ");
            WriteLine("\n\n\n\t\t Twenties: " + twenties.ToString());
            WriteLine("\n\t\t Tens: " + tens.ToString());
            WriteLine("\n\t\t Fives: " + fives.ToString());
            WriteLine("\n\t\t Ones: " + ones.ToString());
            ReadLine();
        }
    }
}
