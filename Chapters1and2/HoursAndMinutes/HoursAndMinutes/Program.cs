﻿using System;
using static System.Console;

namespace HoursAndMinutes
{
    class Program
    {
        static void Main(string[] args)
        {
            int hours = 0;
            int minutes = 0;
            int minutesLeft = 0;

            Write("\n\n\n\t\tPlease enter number of minutes worked: ");
            minutes = Convert.ToInt32(ReadLine());

            hours = minutes / 60;
            minutesLeft = minutes % 60;

            WriteLine("\n\n\n\t\tMinutes Worked: " + minutes + "\n\t\tHours: " + hours + "\n\t\tMinutes: " + minutesLeft);
            ReadLine();
        }
    }
}
